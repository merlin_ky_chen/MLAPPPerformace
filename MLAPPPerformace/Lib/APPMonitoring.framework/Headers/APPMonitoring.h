//
//  APPMonitoring.h
//  APPMonitoring
//
//  Created by 陈克燚 on 2019/11/14.
//  Copyright © 2019 陈克燚. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface APPMonitoring : NSObject

+ (instancetype)defaultCenter;

- (void)enable;

- (void)disable;
#pragma mark - 清除所有本地缓存检测数据
- (void)cleanAllMonitoringData;
@end

NS_ASSUME_NONNULL_END
