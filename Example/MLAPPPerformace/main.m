//
//  main.m
//  MLAPPPerformace
//
//  Created by merlin on 06/12/2020.
//  Copyright (c) 2020 merlin. All rights reserved.
//

@import UIKit;
#import "MLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MLAppDelegate class]));
    }
}
