//
//  MLAppDelegate.h
//  MLAPPPerformace
//
//  Created by merlin on 06/12/2020.
//  Copyright (c) 2020 merlin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
