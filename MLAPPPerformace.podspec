#
# Be sure to run `pod lib lint MLAPPPerformace.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'MLAPPPerformace'
  s.version          = '0.4.0'
  s.summary          = 'APP 性能监控'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitee.com/merlin_ky_chen/MLAPPPerformace'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'merlin' => 'merlin-ky.chen@aia.com' }
  s.source           = { :git => 'https://gitee.com/merlin_ky_chen/MLAPPPerformace.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.subspec 'APPPerformance' do |ss|
      ss.source_files = [
      'MLAPPPerformace/Lib/APPMonitoring.framework/Headers/*.h']

#      ss.frameworks = 'CFNetwork', 'CoreMotion', 'Foundation', 'CoreGraphics', 'SystemConfiguration', 'UIKit', 'CoreText', 'QuartzCore', 'CoreTelephony'
      ss.libraries = 'c++'
      ss.vendored_frameworks =
      'MLAPPPerformace/Lib/*.framework'

      ss.public_header_files = [
      'MLAPPPerformace/Lib/APPMonitoring.framework/Headers/*.h']

  end
  
  # s.resource_bundles = {
  #   'MLAPPPerformace' => ['MLAPPPerformace/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
